# SPDX-FileCopyrightText: 2022 Coop IT Easy SC
#
# SPDX-License-Identifier: MIT

import os
import re
import sys

REGEX = re.compile("Coop IT Easy SCRL\s?fs", re.IGNORECASE)


def replace_scrlfs(path):
    with open(path, "r", encoding="utf-8") as fp:
        text = fp.read()
    new_text = REGEX.sub("Coop IT Easy SC", text)
    with open(path, "w", encoding="utf-8") as fp:
        fp.write(new_text)


def main():
    target_dir = sys.argv[1]
    for root, dirs, files in os.walk(target_dir):
        # Don't mess with git history!
        if ".git" in dirs:
            dirs.remove(".git")
        for file_ in files:
            file_ = os.path.join(root, file_)
            print(file_)
            try:
                replace_scrlfs(file_)
            except UnicodeDecodeError:
                pass


if __name__ == "__main__":
    sys.exit(main())
